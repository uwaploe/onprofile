package main

import (
	"log"

	dputil "bitbucket.org/uwaploe/go-dputil"
	"github.com/gomodule/redigo/redis"
)

// Pass Event messages to a Go channel. If match is not empty or nil, it contains
// the Event names to send.
func eventReader(psc redis.PubSubConn, qlen int, match []string) <-chan dputil.Event {
	c := make(chan dputil.Event, qlen)
	want := make(map[string]bool)
	for _, name := range match {
		want[name] = true
	}
	noFilter := len(want) == 0

	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				ev := dputil.ParseEvent(string(msg.Data))
				if noFilter || want[ev.Name] {
					c <- ev
				}
			}
		}
	}()

	return c
}
