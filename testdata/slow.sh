#!/usr/bin/env bash

mkfifo -m 0666 /tmp/dummy
trap "echo 'sleeping'; sleep 6; rm -f /tmp/dummy; echo 'Done'; exit 0" INT TERM
echo "Starting: mode=${PROFILE_mode}, t=${PROFILE_t}"
read < /tmp/dummy
