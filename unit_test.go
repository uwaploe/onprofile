package main

import (
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/gomodule/redigo/redis"
)

func TestRun(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer s.Close()

	const chName = "events.mmp"

	conn, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}

	psc := redis.PubSubConn{Conn: conn}
	ch := eventReader(psc, 1, nil)
	psc.Subscribe(chName)

	errc := make(chan error, 1)
	go runCmd(ch, time.Second*3,
		[]string{"./testdata/job.sh"},
		errc)
	ev0 := "profile:start t=123456789 mode=up pnum=42"
	ev1 := "profile:end t=123456789 mode=up pnum=42"
	time.Sleep(time.Second * 2)
	s.Publish(chName, ev0)
	time.Sleep(time.Second * 2)
	s.Publish(chName, ev1)

	err = <-errc
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	psc.Unsubscribe()

}

func TestSlowRun(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer s.Close()

	const chName = "events.mmp"

	conn, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}

	psc := redis.PubSubConn{Conn: conn}
	ch := eventReader(psc, 1, nil)
	psc.Subscribe(chName)

	errc := make(chan error, 1)
	go runCmd(ch, time.Second*3,
		[]string{"./testdata/slow.sh"},
		errc)
	ev0 := "profile:start t=123456789 mode=up pnum=42"
	ev1 := "profile:end t=123456789 mode=up pnum=42"
	time.Sleep(time.Second * 2)
	s.Publish(chName, ev0)
	time.Sleep(time.Second * 2)
	s.Publish(chName, ev1)

	err = <-errc
	if err != nil {
		if err.Error() != "signal: killed" {
			t.Errorf("Unexpected error: %v", err)
		}
	}

	psc.Unsubscribe()

}
