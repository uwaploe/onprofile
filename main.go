// Onprofile starts a child process when a Deep Profiler profile starts and
// stops the process when a profile ends.
//
// The following environment variables are set in the child process:
//
//    - PROFILE_mode: profile mode; "up", "down", "docking", or "stationary"
//    - PROFILE_pnum: profile number
//    - PROFILE_t: profile start time in seconds since 1/1/1970 UTC
//
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	dputil "bitbucket.org/uwaploe/go-dputil"
	"github.com/gomodule/redigo/redis"
)

const Usage = `Usage: onprofile [options] command [args ...]

Run command during a Deep Profiler profile.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr      string        = "localhost:6379"
	eventChan   string        = "events.mmp"
	gracePeriod time.Duration = time.Second * 5
	msgQlen     int           = 2
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "addr", rdAddr, "Redis host:port")
	flag.StringVar(&eventChan, "channel", eventChan,
		"Name of Redis channel for profile event messages")
	flag.DurationVar(&gracePeriod, "wait", gracePeriod,
		"time to wait between INT signal and KILL signal for child process")
	flag.IntVar(&msgQlen, "qlen", msgQlen, "size of incoming message queue")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

// Add event parameters to the environment of the child process
func mkEnv(ev dputil.Event) []string {
	env := os.Environ()
	for k, v := range ev.Attrs {
		env = append(env, fmt.Sprintf("PROFILE_%s=%v", k, v))
	}

	return env
}

func runCmd(ch <-chan dputil.Event, wait time.Duration, args []string, errc chan<- error) {
	var (
		cmd *exec.Cmd
	)

	for ev := range ch {
		switch ev.Name {
		case "profile:start":
			if cmd != nil {
				log.Printf("WARNING: %q still running", cmd.Args)
				continue
			}
			if len(args) > 1 {
				cmd = exec.Command(args[0], args[1:]...)
			} else {
				cmd = exec.Command(args[0])
			}
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.Env = mkEnv(ev)
			log.Printf("Starting: %s", cmd.Args)
			if err := cmd.Start(); err != nil {
				log.Printf("Failed: %v", err)
				cmd = nil
				select {
				case errc <- err:
				default:
				}
			} else {
				log.Printf("Running process: %d", cmd.Process.Pid)
			}
		case "profile:end":
			if cmd != nil {
				log.Printf("Stopping: %s", cmd.Args)
				cmd.Process.Signal(syscall.SIGINT)
				done := make(chan bool)
				// Start a goroutine to kill the child process if it
				// doesn't exit within the wait interval.
				go func() {
					timer := time.NewTimer(wait)
					select {
					case done <- false:
						timer.Stop()
						return
					case <-timer.C:
					}
					log.Printf("Killing: %s", cmd.Args)
					cmd.Process.Kill()
					done <- true
				}()
				waitErr := cmd.Wait()
				<-done
				if waitErr != nil {
					log.Printf("Command finished with error: %v", waitErr)
				} else {
					log.Println("Command exited")
				}
				select {
				case errc <- waitErr:
				default:
				}
				cmd = nil
			}
		}
	}
}

func main() {
	args := parseCmdLine()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	// Pub-sub consumer connection
	psconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	log.Printf("Starting onprofile: %s", Version)
	ch := eventReader(psc, msgQlen, []string{"profile:start", "profile:end"})
	psc.Subscribe(eventChan)

	runCmd(ch, gracePeriod, args, nil)
}
