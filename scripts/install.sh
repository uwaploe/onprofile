#!/usr/bin/env bash

BINDIR="$HOME/bin"
PROGDIR="$(pwd)"

files=(*)
for f in "${files[@]}"; do
    [[ -d $f ]] && continue
    ln -sf "$PROGDIR/$f" "$BINDIR"
done
