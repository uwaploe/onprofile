# Deep Profiler Profile Monitor

The `onprofile` program runs a specified command during a Deep Profiler profile. The profile start and stop events are determined by monitoring a Redis pub-sub channel. This program is designed to be run by a service supervisor such as [Runit](http://smarden.org/runit/).
