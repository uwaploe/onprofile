module bitbucket.org/uwaploe/onprofile

go 1.16

require (
	bitbucket.org/uwaploe/go-dputil v1.3.4
	github.com/alicebob/miniredis/v2 v2.15.1 // indirect
	github.com/gomodule/redigo v1.8.5
)
